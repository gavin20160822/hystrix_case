# 第四章 容错组件Hystrix剖析

#### 介绍

- 容错代码作为学习点

PS: 学习PPT等文件在学习群里发放



#### 问题掌握点

1. 项目为什么要做容错？
    
2. 为什么需要hystrix等类似组件？

3. Hystrix如果解决依赖隔离？

4. 线程池隔离的优缺点？

5. 信号量隔离的优缺点？





Software Architecture Software architecture description
Instructions 课堂笔记及代码归档 Gitee Feature