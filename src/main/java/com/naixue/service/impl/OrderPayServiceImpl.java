package com.naixue.service.impl;

import java.util.concurrent.Future;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.naixue.entity.OrderPay;
import com.naixue.service.OrderPayService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.command.AsyncResult;

@Service
public class OrderPayServiceImpl implements OrderPayService {

	@Override
	public OrderPay get(Long id) {
		return null;
	}

	@Override
	@HystrixCommand(fallbackMethod = "orderError")
    public Future<OrderPay> orderC1(Long id) {
        return new AsyncResult<OrderPay>() {
            @Override
            public OrderPay invoke() {
            	int i = (int) (1/id);
            	System.out.println("接受ID：" + id + "结果" + i);
            	OrderPay orderPay = new OrderPay();
				orderPay.setMoney(9999.99);
				orderPay.setId(id);
				return orderPay;
            }
            // 重写了 get 方法			重写继承自 AsyncResult 类的get 方法!
 			@Override
 			public OrderPay get() {
                 return invoke();
 			}
        };
    }
	
	
	public OrderPay orderError(@PathVariable("id") Long id, Throwable throwable) {
		 //fallback逻辑
		System.out.println("异常信息：" + throwable.getMessage());
		OrderPay orderPay = new OrderPay();
		orderPay.setHsDesp("-=====");
		return orderPay;
	}

}
