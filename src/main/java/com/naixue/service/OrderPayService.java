package com.naixue.service;

import java.util.concurrent.Future;

import com.naixue.entity.OrderPay;

public interface OrderPayService {

	OrderPay get(Long id);

	Future<OrderPay> orderC1(Long id);

}
